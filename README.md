# appstream-debian-yaml

Partial extraction DEP-11 AppStream data from Debian by the by the script [appstream-debian-to-yaml](https://git.framasoft.org/codegouv/appstream-debian-to-yaml).
